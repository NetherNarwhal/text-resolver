"use strict";
// v1.0.3

// Random number generator based on Johannes Baagøe's Alea implementation from https://github.com/davidbau/seedrandom.
class InternalRandom {
    constructor(seed) {
        if (seed === undefined) seed = Math.random();
        this.seed = seed;
        this.c = 1;
        this.n = 0xefc8249d;
        this.s0 = this.m(' ');
        this.s1 = this.m(' ');
        this.s2 = this.m(' ');
        this.s0 -= this.m(seed);
        if (this.s0 < 0) this.s0 += 1;
        this.s1 -= this.m(seed);
        if (this.s1 < 0) this.s1 += 1;
        this.s2 -= this.m(seed);
        if (this.s2 < 0) this.s2 += 1;
    }
    next() {
        let t = 2091639 * this.s0 + this.c * 2.3283064365386963e-10; // 2^-32
        this.s0 = this.s1;
        this.s1 = this.s2;
        return this.s2 = t - (this.c = t | 0);
    }
    m(data) {
        data = String(data);
        for (let i = 0; i < data.length; i++) {
            this.n += data.charCodeAt(i);
            let h = 0.02519603282416938 * this.n;
            this.n = h >>> 0;
            h -= this.n;
            h *= this.n;
            this.n = h >>> 0;
            h -= this.n;
            this.n += h * 0x100000000; // 2^32
        }
        return (this.n >>> 0) * 2.3283064365386963e-10; // 2^-32
    }
}

// This is the an class that you should be using.
class Resolver {
    constructor() {
        this.defaultOrigin = "origin"; // If not text is provided to resolve we will look for this property on the context to pull it from.
    }
    resolve(context, text, seed, cloneContext=true) {
        if (!context) {
            context = {}; // This doesn't seem particularly useful as a default. Perhaps this should be an error?
        } else if (cloneContext) {
			// WARNING: This is not a deep copy, just a shallow one. Also, may not work if they have messed with their prototype after creating their instance.
            let newCtx = Object.create(Object.getPrototypeOf(context)); // Create a new instance of same prototype to get access to functions from it (ex. custom class).
            context = Object.assign(newCtx, context); // Copy over old object attributes to new one.
        }
        if (!text) text = context[this.defaultOrigin]; // Look for an default property in the context to pull from if no starting text is provided.
        if (!text || text.length === 0) throw Error(`No starting text provided. Tried to use the '${this.defaultOrigin}' property in the provided context as a fallback, but that was missing too.`);

        context.rnd = new InternalRandom(seed);
        this.lastSeed = context.rnd.seed;
        let out = this._resolve(context, text).trim();
        out = out.replace(/  +/g, ' '); // Reduce sequential multiple spaces to one.
        out = out.replace(/\s+([.,:;!?])/g, "$1"); // Remove spaces in front of punctuation (periods, etc).
        return out;
    }
    _resolve(context, text) {
        if (text === undefined || text.length === 0) return "";
        if (typeof text !== "string") text = text.toString(); // If they didn't provide a String try to turn it into one.
        let out = "";
        let anInject = 0; // Used to track whether the next word should be prefixed with an "A" or "An" and whether it should be capitalized or not.
        let end = 0;
        let start = this.getNextDelimiter(text, end);
        while (start >= 0) {
            out += text.substring(end, start); // Output what is in the text from where we last pulled from.
            // Determine the type of dynamic content and extract it.
            let delim = text.charAt(start);
            if (delim === '{') { // See if it is a variable substitution.
                end = this.extractVariables(text, start + 1);
                let varText = text.substring(start + 1, end).trim();
                if (varText === 'A') {
                    anInject = 1
                    if (text.charAt(end + 1) === ' ') end++; // Skip it if they have a trailing space as we'll add with the a|an inject.
                } else if (varText === 'a') {
                    anInject = 2;
                    if (text.charAt(end + 1) === ' ') end++; // Skip it if they have a trailing space as we'll add with the a|an inject.
                } else if (varText === 's') {
                    out = this.plural(out); // Find previous word in output and pluralize it. If out is empty then ignore.
                } else if (varText.indexOf(':') > -1) {
                    this.setVariable(context, varText); // This is a variable assignment.
                } else { // This is a variable read.
                    let val = this.resolveAttribute(context, varText).toString(); // toString handles if the variable was a number.
                    val = this._resolve(context, val); // We do here rather than in the resolveAttr because things like setVariable may want an object.
                    if (val && val.length > 0) { // Only reset if there was actually something the a|an was applied to.
                        out += this.handleIndefiniteArticle(val, anInject); // Parse it due to nesting.
                        anInject = 0;
                    }
                }
            } else if (delim === '[') {
                let ret = this.extractOptions(text, start + 1);
                let option = this.selectOption(context, ret.options); // Pick one and see if any of the options have conditions that don't apply.
                let val = this._resolve(context, option);
                if (val && val.length > 0) { // Only reset if there was actually something the a|an was applied to.
                    out += this.handleIndefiniteArticle(val, anInject); // Pick one of the available options and then parse it due to nesting.
                    anInject = 0;
                }
                end = ret.end;
            }
            end++; // Position us at the end of the dynamic part.
            start = this.getNextDelimiter(text, end); // Look for the next dynamic part.
        }
        out += this.handleIndefiniteArticle(text.toString().substring(end), anInject); // Append what is left of the input text (it won't always be a String, so convert).
        return out;
    }
    setVariable(context, varText) {
        let args = varText.split(':');

        // Get the variable to set.
        let variable = args[0];
        let localContext = context;
        if (variable.indexOf('.') >= 0) {
            let attributes = variable.split('.');
            for (let i = 0; i < attributes.length - 1; i++) {
                let attribute = attributes[i];
                // Handle nested attributes. If we can't find it in the current context then create it.
                let tempVar = this.resolveAttributeValue(context, localContext, localContext[attribute], attribute, true);
                if (tempVar === undefined) {
                    localContext[attribute] = {}; // Parent object must not exist, so we will helpfully create it.
                    localContext = localContext[attribute];
                } else {
                    localContext = tempVar;
                }
                variable = attributes[i + 1];
            }
        }

        // Get the value to set it to.
        let val = args[1];
        if (val.length === 0 || val == "undefined") {
            val = undefined; // Handle the special case where they are setting it to be undefined.
        } else if (val == "empty") {
            val = ""; // Handle the special case where they are setting it to an empty string.
        } else if (val.charAt(0) === '$') {
            // If the value starts with '$', then don't resolve it. This allows a variable to have different output each time.
            val = val.substring(1); // Chop off the '$'.
        } else if (val.charAt(0) === '@') {
            // The statement is a reference to a variable.
            val = val.substring(1);
            val = this.resolveAttribute(context, val); // Try to resolve the value as if it is a variable.
        } else {
            // For a value containing decisions, variables, or other dynamic content we need to fully resolve it and store that in the variable.
            // This allows referencing the same item or location multiple times.
            val = this._resolve(context, val);
        }
        localContext[variable] = val;
    }
    resolveAttribute(context, attribute, ignoreMissing=false) {
        let val = context;
        if (attribute.indexOf('.') >= 0) { // Detect nested attributes (ex. {hero.name}).
            let attributes = attribute.split('.');
            for (let i = 0; i < attributes.length; i++) {
                attribute = attributes[i];
                if (attribute.length === 0) continue;
                if (attribute.length === 1) { // Assume any 1 letter property is actually a modifier.
                    if (attribute === 's') {
                        val = this.plural(val); // Pluralize the value.
                    } else if (attribute === 'c') {
                        val = this.capitalize(val); // Capitalize the value.
                    } else if (attribute === 't') {
                        val = this.titlecase(val); // Titlecase the value.
                    } else if (attribute === 'l') {
                        val = this.lowercase(val); // Lowercase the value.
                    } else if (attribute === 'u') {
                        val = this.uppercase(val); // Uppercase the value.
                    } else if (attribute === 'b') {
                        val = this.getFirstWord(val); // Get just the beginning (first) word from the value.
                    } else if (attribute === 'e') {
                        val = this.getLastWord(val); // Get just the ending (last) word from the value.
                    }
                } else {
                    val = this.resolveAttributeValue(context, val, val[attribute], attribute, ignoreMissing);
                }
                if (val === undefined) return;
            }
        } else {
            val = this.resolveAttributeValue(context, context, context[attribute], attribute, ignoreMissing);
        }
        return val;
    }
    // Need both global and local contexts. Local is for parsing the individual variable (ex. house is relative to bob in {bob.house}).
    // Global is for resolving the values of those variables as they could be variables themselves (ex. color is off root of context in {bob.house} where house is "a {color} house" not relative to bob).
    resolveAttributeValue(globalCtx, localCtx, val, attrName, ignoreMissing) {
        if (val === undefined) {
            if (ignoreMissing) return val;
            throw Error(`Undefined attribute '${attrName}'. The variable may be misspelled.`);
        }
        if (val instanceof Function) { // If variable is a function, then get the results of the function.
            let func = val.bind(localCtx); // OOP in messed up in JS. If this is a function on a class then we need to bind it to the class.
            val = this.resolveAttributeValue(globalCtx, localCtx, func(), attrName + " (nested)");
        } else if (Array.isArray(val)) {
            let rnd = Math.floor(globalCtx.rnd.next() * val.length);
            val = val[rnd]; // If variable is an Array, then return one of its values randomly.
        }

        // Resolve any string parameters.
        if (typeof val == "string") {
            return this._resolve(globalCtx, val);
        } else {
            return val;
        }
    }
    getNextDelimiter(text, start) {
        for (let i = start; i < text.length; i++) {
            let c = text.charAt(i);
            if (c === '{' || c === '[') return i;
        }
        return -1;
    }
    // Returns the end of the variable statement, noting that they could be nest variables inside variables.
    extractVariables(text, start) {
        let indent = 0;
        for (let i = start; i < text.length; i++) {
            let c = text.charAt(i);
            if (c === '{') {
                indent++;
            } else if (c === '}') {
                indent--;
                if (indent < 0) return i;
            }
        }
        throw Error(`Couldn't find the end of the variable declaration. Check that the number of {'s and }'s match. index=${indent}, start=${start}, text='${text}'.`);
    }
    // Parse out the options while accounting for possible nesting.
    extractOptions(text, start) {
        let indent = 0;
        let options = [];
        for (let i = start; i < text.length; i++) {
            let c = text.charAt(i);
            if (c === '|') {
                if (indent === 0) {
                    options.push(text.substring(start, i));
                    start = i + 1;
                }
            } else if (c === '[') {
                indent++;
            } else if (c === ']') {
                indent--;
                if (indent < 0) {
                    options.push(text.substring(start, i));
                    return {"options": options, "end": i };
                }
            }
        }
        throw Error(`Couldn't find the end of the decision block. Check that the number of ['s and ]'s match. index=${indent}, start=${start}, text='${text}'.`);
    }
    // Removes any options that have conditions that do not match. This changes the passed list of options.
    selectOption(context, options) {
        let percents;
        let percentTotal = 0;
        let hadPercents = 0;
        for (let i = options.length - 1; i >= 0; i--) {
            let option = options[i];
            if (option.length === 0) continue;
            let c = option.charAt(0);

            if (c === '@') {
                let cond = this.getFirstWord(option.substring(1));
                let truncLen = cond.length + 2; // Cache as cond can change in the logic below before we need this.
                // TODO: We may end up with more complicated conditions ex. [@x=Bob test|@x!=Bob test|@x<2 test|@x>=5 test].
                // Support invert notation ex. [@!Bob test].
                let invert = false;
                if (cond.charAt(0) === '!') {
                    invert = true;
                    cond = cond.substr(1); // Remove the ! from the front.
                }
                let val = this.resolveAttribute(context, cond, true);
                // Evaluate if the value is true.
                let valid = val;
                if (invert) valid = !valid;
                if (valid) {
                    options[i] = option.substr(truncLen); // Chop off the condition from the option so it doesn't get in the output.
                } else {
                    options.splice(i, 1); // Value isn't true, so remove this option from those available.
                    if (percents) percents.splice(i, 1); // Remove from percents array too or they won't match.
                }

            } else if (c === '%') {
                // Parse out the percentage.
                let word = this.getFirstWord(option.substring(1));
                let percent = parseFloat(word);
                if (!isFinite(percent)) throw Error(`Unable to parse the percentage in '${option}'. Please check that the syntax is correct, %## followed by a space.`);
                if (percent <= 0 || percent >= 100) throw Error(`Percentages must be greater than 0 and less than 100. '${percent}' was specified in '${option}'.`);

                // Keep track of it.
                if (!percents) percents = [];
                percents[i] = percent;
                percentTotal += percent;
                hadPercents++;

                // Remove the percentage from the option text.
                options[i] = option.substr(word.length + 2);
            }
        }
        if (!percents) {
            let rnd = Math.floor(context.rnd.next() * options.length);
            return options[rnd]; // Not percentages specified, so just pick an option at random.
        }

        // Return an option based on weighted random using the percentages specified.
        let sum = 0;
        let defaultPercent = 0;  // Setting as a separate if check to avoid divide by 0 if every option had a percent specified.
        if (options.length > hadPercents) defaultPercent = (100 - percentTotal) / (options.length - hadPercents);
        let rnd = context.rnd.next() * 100;
        for (let i = 0; i < options.length; i++) { // Loop on options, not percents. Percents won't have same number of elements if the last option didn't have a percentage defined.
            sum += (percents[i] === undefined ? defaultPercent : percents[i]);
            if (rnd < sum) return options[i];
        }
        return; // Only get here if the percents didn't add up to 100 (were no defaults which would have covered the range) and the random number went high. This is ok and even desired sometimes (see tests).
    }
    getFirstWord(text) {
        if (text === undefined) return text;
        text = text.trim();
        if (text.length === 0) return text;
        let end = text.indexOf(' ');
        if (end > -1) text = text.substring(0, end);
        return this.removePunctuationAtEnd(text);

        // Alternatively, we could just do this. It may be slower, but it does cleanly avoid any trailing punctuation and doesn't need a trim.
        // let matched = text.match(/[a-zA-Z0-9_']+/); // Same as /\w+/, but keeps apostrophes too.
        // if (matched) return matched[0];
        // return text; // Not sure what was actually in this, but doesn't seem to be a word.
    }
    getLastWord(text) {
        if (text === undefined) return text;
        text = text.trim();
        if (text.length === 0) return text;
        let end = text.lastIndexOf(' ');
        if (end > -1) text = text.substring(end + 1);
        return this.removePunctuationAtEnd(text);

        // Alternatively we could just do this. It may be slower, but it does cleanly avoid any trailing punctuation and doesn't need a trim.
        // let match = text.match(/^.*?(\w+)\W*$/, "$1"); // Note: Doesn't handle words with apostrophes in them (splits them at the apostrophe). Try /^.*?([a-zA-Z0-9_']+)[^a-zA-Z0-9_']*$/
        // if (matched) return matched[1]; // 0 is everything before the last word, so we want 1.
        // return text; // Not sure what was actually in this, but doesn't seem to be a word.
    }
    removePunctuationAtEnd(word) {
        let c = word.charAt(word.length - 1);
        if (c === '.' || c === ',' || c === ';' || c === '!' || c === '?') word = word.substring(0, word.length - 1); // Strip off trailing punctuation.
        return word;
    }
    isVowel(c) {
        // return ['a', 'e', 'i', 'o', 'u'].indexOf(c.toLowerCase()) !== -1; // This works too. Which approach is faster?
        c = c.toLowerCase();
        if (c === 'a' || c === 'e' || c === 'i' || c === 'o' || c === 'u') return true; // No "sometimes y".
        return false;
    }
    handleIndefiniteArticle(text, flag) {
        // Technically, the decision is based on whether the first syllable SOUNDS like a vowel. That is pretty complex so cheating and going with whether the first letter is a vowel or not.
        // See https://github.com/EamonNerbonne/a-vs-an or https://github.com/rigoneri/indefinite-article.js
        if (!flag) return text;
        let isCap = (flag === 1);
        if (!text) return ((isCap) ? "A" : "a");
        text = text.trim();
        if (text.length === 0) return ((isCap) ? "A" : "a");

        // Handle a few common cases.
        let word = this.getFirstWord(text);
        if (word === "use" || word === "used" || word === "one") return ((isCap) ? "A " : "a ") + text;
        if (word === "hour" || word === "honest") return ((isCap) ? "An " : "an ") + text;

        // Fall back to the quick, but sometimes inaccurate, vowel test.
        if (this.isVowel(word.charAt(0))) return ((isCap) ? "An " : "an ") + text;
        return ((isCap) ? "A " : "a ") + text;
    }
    plural(word) {
        // FIXME: Assumes we are provided just a word. What if it is a sentence? Use this.getLastWord(), but if we do how do we rebuild the string to return (likely slice front based on length of last)?
        // There are way more corner cases than this. Cheating and handling the most common cases only. See https://github.com/blakeembrey/pluralize for a more advanced solution.
        if (word === undefined || word.length === 0 || word.trim().length === 0) return word;
        // Handle a few irregular words.
        if (word === "foot") return "feet";
        if (word === "goose") return "geese";
        if (word === "child") return "children";
        if (word === "tooth") return "teeth";
        if (word === "person") return "people";
        if (word === "hero") return "heroes";
        // Handle a few uncountable words
        if (word === "food" || word === "dice" || word === "sheep" || word === "clothing" || word === "fish" || word === "deer" || word === "moose" || word === "series" || word === "equipment") return word;

        let last = word.charAt(word.length - 1);
        if (last == 's' || last == 'x') {
            if (word == "Ox") return "Oxen";
            if (word.endsWith("es")) return word; // If it is "es" we assume it is already plural.
            return word + "es";
        }
        if (last == 'y') {
            let c = word.charAt(word.length - 2);
            if (c == 'a' || c == 'e') return word + "s";
            return word.substring(0, word.length - 1) + "ies";
        }
        if (last == 'f') {
            if (word == "Coif") return "Coifs";
            if (word.charAt(word.length - 2) == 'f') return word.substring(0, word.length - 2) + "ves";
            return word.substring(0, word.length - 1) + "ves";
        }
        if (last == 'h') {
            if (word == "Pharaoh") return "Pharaohs";
            let c = word.charAt(word.length - 2);
            if (c == 't' || c == 'p') return word + "s";
            return word + "es";
        }
        if (word.endsWith("man")) return word.substring(0, word.length - 2) + "en";
        return word + "s";
    }
    // A.k.a. Sentencecase the first word (only the first letter is uppercase, as in a sentence).
    capitalize(word) {
        if (word === undefined) return;
        word = word.trimStart();
        if (word.length === 0) return word;
        return word.charAt(0).toUpperCase() + word.substring(1);
    }
    // Treat the text as a title, capitalizing most of the words within.
    titlecase(text) {
        if (text === undefined) return;
        text = text.trimStart();
        if (text.length === 0) return text;
        text = text.replace(/([^\W_]+[^\s-]*) */g, function(txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase() });
        // Some words should be lowercase unless they are the first or last words.
        let lowers = ["A","An","The","And","But","Or","For","Nor","As","At","By","For","From","In","Into","Near","Of","On","Onto","To","With"];
        for (let i = 0, j = lowers.length; i < j; i++) {
            text = text.replace(new RegExp('\\s' + lowers[i] + '\\s', 'g'), function(txt) { return txt.toLowerCase() });
        }
        return text;
    }
    lowercase(text) {
        if (text === undefined) return;
        text = text.trimStart();
        if (text.length === 0) return text;
        return text.toLowerCase();
    }
    uppercase(text) {
        if (text === undefined) return;
        text = text.trimStart();
        if (text.length === 0) return text;
        return text.toUpperCase();
    }
}