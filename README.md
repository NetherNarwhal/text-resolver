# Text Resolver
Text Resolver is a tool for generating text given a series of input strings that get substituted into each other like an overly complicated [Mad Libs](https://en.wikipedia.org/wiki/Mad_Libs). While the output will always just be pieces and parts of the input, the various combinations can result in interesting outcomes particularly when there are many variations.

This project came into being as ES6 String templates were limited mostly to resolving at startup which prevented repeating generation. This project introduced simple string substitutions, then inline decisions, etc. I initially started writing it as an actual parser, building a tree, etc, but opting to go with inline handling since the feature set was pretty small at the time and it allowed flexibility if I changed content while parsing. It isn't ideal, but it gets the job done.

I built Grammar around the same time as [Tracery](https://github.com/galaxykate/tracery) and [Twine](https://twinery.org/), although it isn't really based on either of them. They are excellent, and likely better, examples of this type of functionality so I recommend checking them out. Also, I've recently come across [Chartopia](https://chartopia.d12dev.com), and its [documentation](https://chartopia.d12dev.com/docs), which I think is pretty cool however it is a much more of a dice and table based approach.

## The Basics
Text Resolver is a basic js file, so you just need to reference as needed, such as `<script src="<your path>/grammar.js"></script>` in html, then declare an instance of the class via `let resolver = new Resolver()`. You can reuse the same instance for all of the text generation calls.

To use the resolver, you'll need to pass in a context object along with an optional starting string. If the starting string is not provided then the resolver will attempt to get one from an property called `origin` on the provided context. The context object should contain all of the text snippets that you will want to use. Here is a simple example using variables and decision blocks.

```javascript
let ctx = { hero: "Bob", monster: { name: "iguana", size: "[huge|big|small|tiny]" }, color: ["red","green"], };
let resolver = new Resolver();
console.log(resolver.resolve(ctx, "This is a [story|tale|fable] of {hero} and the {monster.size} [%25 {color}] {monster.name}."));
// Will produce something like: This is a tale of Bob and the tiny red iguana.
```

Some caveats:
* The output will be trimmed and have duplicate spacing around variables and decisions removed. 
* Your input text may not include any square or curly brackets (`[` or `{`) except to define variables and decisions. Unfortunately, there is not currently any way to escape them.
* Text without variables or decisions will be, aside from the trimming and de-duplication of spaces, returned unaltered.
* The provided context will be (shallow) cloned when resolving so that temporary variables are not persisted. This allows the same context to be reused.
* Due to modifiers, you shouldn't have properties on your objects in the context called `a`, `b`, `e`, `l`, `u`, `t`, `s`. They won't cause failures, but you'll never be able to access them through the variable syntax as it will invoke the modifier instead.
* It resolves recursively and dynamically, so if you build something with a lot of nested variables and decisions you may encounter Javascript recursion limits. I've never had this happen so you'd really have to be pushing things, but it is theoretically possible.

## Variables
The provided text may reference properties of the context by wrapping them in curly brackets, `{` and `}`, such as `{hero}` will return the output of a property on the context called `hero`. Based on what the property's value will be evaluated as follows:
* If the variable is an array, then one of the elements will be randomly picked as the output.
* If the variable is a function, then it will be invoked with no parameters and the output is what is returned.
* For anything else, it will call `toString` on the returned value to try to turn it into a `String`.

It is valid to reference nested properties of the context, such as `hero.weapon`, however you cannot reference a specific array element. For instance, `hero.inventory[0]` is not supported. All resolving of properties and nested properties is done from the root of the context, not from the property that returned the value. For instance, if `hero.weapon` returns `"{color}"`, color will assume to refer to `color`, not `hero.weapon.color`. Basically, you need to fully qualify any variables as if they were from the root of the context.

You may automatically create a new variable (property) on the context with the name provided using `{myprop:some value}`. This will create, or overwrite if it already exists, a property called `myprop` on the context and set its value to `"some value"`. Likewise, you may also set a nested property using `{myprop.another:something}`.

There are two other special cases for settings variables.
* The default approach described above is great for fully evaluating the provided value and storing it for later. For instance, you can generate a location name and store it to reference it multiple times in your output text.
* By specifying the no resolve flag, `$`, at the beginning of the value tells the resolver just to copy the value over without trying to resolve it. If it contains a reference to a decision you could get a different value each time it is used. This lets you create dynamic text fragments for later use just like you defined in your input.
* By specifying the object flag, `@`, at the beginning of the value tells the resolver to have that variable reference an object. This is useful for aliasing or caching references to objects that you still want to access properties of.

Examples of the above.
```javascript
let ctx = { dragon: "Smaug", hero: { name: "Bilbo", }, };
let resolver = new Resolver();
// Reading from a variable.
console.log(resolver.resolve(ctx, "{hero.name} is taking gold from {dragon}.")); // Bilbo is taking gold from Smaug.
console.log(resolver.resolve(ctx, "{hero.pocket:ring} {hero.name} has a {hero.pocket} in his pocket.")); // Bilbo has a ring in his pocket.
console.log(resolver.resolve({ dragonColor: "{color}", color: "red"}, "Smaug is {dragonColor}")); // Variable returns a variable: Smaug is red

// Setting a variable, where the value being set is fully resolved and cached as a string in the variable.
console.log(resolver.resolve({color: "red"}, "{val:{color}} Smaug is {val}")); // Set variable to a variable: Smaug is red
console.log(resolver.resolve({color: "red"}, "{val:Smaug is {color}} {val}")); // Set variable to a string with a variable: Smaug is red
console.log(resolver.resolve({}, "{color:[red|green|blue|black]} Smaug is {color} {color} {color} {color} {color}")); // Value is cached.
console.log(resolver.resolve({}, "{desc:Smaug is [red|black]} {desc} {desc} {desc} {desc} {desc}")); // Value is cached.
console.log(resolver.resolve({color: "[red|green|blue|black]"}, "{desc:Smaug is {color}} {desc} {desc} {desc} {desc} {desc}")); // Value is cached.

// Setting a variable using '$', where the value should be copied into the variable as-is (no resolution, no result caching).
console.log(resolver.resolve({}, "{color:$[red|green|blue|black]} {color} {color} {color} {color} {color}")); // Value is NOT cached.
console.log(resolver.resolve({ ball: { color: "[red|black]"} }, "{dragon:ball} Smaug is {dragon.color}.")); // Error! dragon is set to "ball".
console.log(resolver.resolve({ ball: { color: "[red|black]"} }, "{dragon:${ball}} Smaug is {dragon.color}.")); // Error! dragon is set to "object Object".
console.log(resolver.resolve({ ball: { color: "[red|black]"} }, "{dragonColor:${ball.color}} Smaug is {dragonColor}.")); // Value is NOT cached.

// Setting a variable using '@', where the variable is being set to a reference to another object.
console.log(resolver.resolve({ ball: { color: "[red|black]"} }, "{dragon:@ball} Smaug is {dragon.color}.")); // Variable points to another variable.
console.log(resolver.resolve({ ball: { color: "[red|black]"} }, "{dragonColor:@ball.color} Smaug is {dragonColor}.")); // Variable points to another variable.
```

## Decisions
There will often be times when you want the resolver to randomly pick a value to use. This can be done using a variable pointing to an array of values, but you can also declare the options inline within your text by wrapping the options in square brackets, `[` and `]`. Each option listed will have an equal chance unless you specify a specific percentage, such as `%20`. You may also include empty options or options that appear only if a variable exists, using `@`, or not. You may provide as many options as you like within this structure and nest decisions within decisions.

Examples of the above.
```javascript
let ctx = { name: "Spike" };
let resolver = new Resolver();
console.log(resolver.resolve(ctx, "The dog is [hot|cold].")); // 50% hot, 50% cold
console.log(resolver.resolve(ctx, "The dog is [%30 hot|cold].")); // 30% hot, 70% cold
console.log(resolver.resolve(ctx, "The dog is [%30 hot|warm|cold].")); // 30% hot, and rest are divided between 70% (35% warm, 35% cold)
console.log(resolver.resolve(ctx, "The dog is [%80 hot|%50 warm|cold].")); // over 100%, so 80% hot, 20% warm, 0% cold
console.log(resolver.resolve(ctx, "The dog is [hot|].")); // when hot isn't selected nothing is returned due to empty option.
console.log(resolver.resolve(ctx, "The dog is [%50 hot].")); // same as empty option above, but more explicit.
console.log(resolver.resolve(ctx, "The dog's name is [@name {name}|unknown].")); // Returns context.name if it exists (or is not false).
console.log(resolver.resolve(ctx, "The dog's name is [@!name {name}|unknown].")); // Won't return context.name if it exists.
```

Currently, the only condition test is whether the variable exists, so you can not make a decision based on its value. This may be added in the future.

## Modifiers
Modifiers can be used to change the output to: pluralize it (dog -> dogs), capitalize (uppercase the first letter, rest are lowercase), titlecase (capitalize the words as in a book title), lowercase, uppercase, set the correct indefinite article (a or an), return just the beginning/first word, return just the end/last word.

```javascript
let ctx = { text: "heLlo wOrlD" };
let resolver = new Resolver();
console.log(resolver.resolve(ctx, "{text.b}")); // Beginning (first) word: heLlo
console.log(resolver.resolve(ctx, "{text.e}")); // Ending (last) word: wOrlD
console.log(resolver.resolve(ctx, "{text.l}")); // Lowercase: hello world
console.log(resolver.resolve(ctx, "{text.u}")); // Uppercase: HELLO WORLD
console.log(resolver.resolve(ctx, "{text.t}")); // Titlecase: Hello World
console.log(resolver.resolve(ctx, "{text.s}")); // Puralize it: heLlo wOrlDs
console.log(resolver.resolve(ctx, "Dog{s}")); // Puralize current output: Dogs
console.log(resolver.resolve(ctx, "[Dog|Cat]{s}")); // Puralize current output: Dogs or Cats
console.log(resolver.resolve(ctx, "{A} dog")); // Indefinite article (upper) for following word: A dog
console.log(resolver.resolve(ctx, "{a} ape")); // Indefinite article (lower) for following word: an ape
console.log(resolver.resolve(ctx, "{text.t.s}")); // Combination: Hello Worlds
```
Caveats:
* Due to modifiers, you shouldn't have properties on your objects passed to the resolver called `a`, `b`, `e`, `l`, `u`, `t`, `s`. They won't cause failures, but you'll never be able to access them through the variable syntax as it will think it is a modifier instead.
* The logic in the resolver for indefinite articles just looks at the first letter of the following word, which isn't technically correct. The official rule is to go based on whether the first syllable sounds like a vowel which is significantly harder to check for and isn't always consistent. I may pull in something like the [A-vs-An](https://github.com/EamonNerbonne/a-vs-an) or [Indefinite Article](https://github.com/rigoneri/indefinite-article.js) projects down the road if it becomes an issue.
* Pluralize handles only the common cases. There are many unique cases in English, including cases where the words are completely different such as person to people, that may not be handled. I may pull in something like the [Pluralize project](https://github.com/blakeembrey/pluralize) down the road if it becomes an issue.

## Random Seeds
When computer programs generate random numbers they typically use algorithms involving a [random seed](https://en.wikipedia.org/wiki/Random_seed). By default, the resolver will generate random numbers using a new random seed each time. This provides output that typically changes each time. If you wish to produce consistent results on each run you may specify a seed to use at the start of the resolve. If you find output you like you can also retrieve the seed that was used and pass that in again. Be warned that any change to the order or number of decisions or their options could change the results, even if using the same seed each time.

The implementation of the random number generator used is based on Johannes Baagøe's Alea implementation from the [seedrandom project](https://github.com/davidbau/seedrandom).

```javascript
let resolver = new Resolver();

// Retrieve the seed of a previous run
console.log(resolver.resolve({ color: "[red|green|blue|black]" }, "{color}"));
console.log(resolver.lastSeed); // Get the seed that was used to produce the output above

// Provide a seed for random number generation
let seed = "anything"; // Can be any data type, but test is usually the easiest to remember
console.log(resolver.resolve({ color: "[red|green|blue|black]" }, "{color}", seed));
console.log(resolver.resolve({ color: "[red|green|blue|black]" }, "{color}", seed)); // Same output as above
console.log(resolver.resolve({ color: "[red|green|blue|black]" }, "{color}", seed)); // Same output as above
console.log(resolver.resolve({ color: "[red|green|blue|black]" }, "{color}", seed)); // Same output as above
console.log(resolver.resolve({ color: "[%70 orange|red|green|blue|black]" }, "{color}", seed)); // Output likely different due to changed input
```

## Pronouns
Politics aside, pronouns are difficult to handle correctly for English. However, they are unavoidable in text generation as people and things play large parts. I haven't found a foolproof way to handle these automatically in the resolver, but here are some tips I've used.

Table of Pronouns from [Rantionary](https://github.com/RantLang/Rantionary/blob/master/pronouns-third-person.table)

|                  | Subjective | Objective | Reflexive  | Possessive | Absolute Possessive |
| ---------------- |:----------:|:---------:|:----------:|:----------:|:-------------------:|
| Singular Male    | he         | him       | himself    | his        | his                 |
| Singular Female  | she        | her       | herself    | her        | hers                |
| Plural           | they       | them      | themselves | their      | theirs              |
| Thing            | it         | it        | itself     | its        | its                 |

To handle these, I will typically create a character class for any references to people. On that class I then have properties named for the plural properties (they, them, etc) initialized with values from gender during construction. For example, if male then the `their` property returns `his`. Then when used in the text, I can just type `"The hero grabbed {hero.their} sword."` and either `his` or `her` gets plugged in during generation. I've found this to be an easy way to handle as I don't need to remember the technical terms for which to use and the plural version is unique for each category. If I named the methods with the male or female versions there would be ambiguity in the mapping. This is because "his" occurs twice for males, "her" occurs twice for females, and it repeats both "it" and "its".

Here is the class that I use. I also include a `toString` method so I don't have to refer to the character's name property all the time. You can also use that to automatically apply titles (Lord, Lady, etc), species (the Tiger, the Pony, etc), or whatever to the name.
```javascript
class Character {
    constructor (name, gender) {
        this.name = name;
        this.gender = gender;

        // Use the actual word rather than the term for it as it is easier to remember when typing. ex. {hero.they}
        // I use the plural form for this attribute name because it is unambiguous (male repeats "his", female repeats "her").
        if (gender === "male") {
            this.they = "he"; // subjective
            this.them = "him"; // objective
            this.themselves = "himself"; // reflexive
            this.their = "his"; // possessive
            this.theirs = "his"; // absolute
        } else {
            this.they = "she"; // subjective
            this.them = "her"; // objective
            this.themselves = "herself"; // reflexive
            this.their = "her"; // possessive
            this.theirs = "hers"; // absolute
        }
    }

    toString() {
        return this.name;
    }
}
```

## TODOs
Ideas for potential features I may add down the road, although at this point I think what is here is good enough so no promises.
* Ways to make debugging easier.
* Allow for more complex conditions. ex. `[@x=Bob test|@x!=Bob test|@x<2 test|@x>=5 test]`. Possibly compound ones (&& and ||).
* Allow percentages to go over 100, but assign the default to be equal to the smallest amount that is specified. ex. `[%80 dog|%30 cat|%10 frog|lizard]` where lizard would get %10 and random is out of 130.
* Support for array indexes ex. `{@hero[1] text}`.
* Something to track if a given option has been used before and to not select it again. This gets pretty complicated to handle generically.
* Support for a markov generator command that takes in an array. Wouldn't be very efficient as it would need to be loaded each time (maybe cache in context by name of variable passed?). [Good base for an implementation](http://www.roguebasin.com/index.php?title=Names_from_a_high_order_Markov_Process_and_a_simplified_Katz_back-off_scheme).